<?php
/**
 * MyThemeName Theme Customizer
 *
 * @package MyThemeName
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function MyThemeName_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'police_de_caracteres', array(
			'title'       => __( 'Fonts', 'MyThemeName' ),
			'priority'    => 25,
		) );
		$font_choices = array(
			'Georgia, serif' => 'Georgia',
			'"Palatino Linotype", "Book Antiqua", Palatino, serif' => 'Palatino',
			'"Times New Roman", Times, serif' => 'Times',
			'Arial, Helvetica, sans-serif' => 'Arial',
			'"Arial Black", Gadget, sans-serif' => 'Arial Black',
			'"Comic Sans MS", cursive, sans-serif' => 'Comic Sans MS',
			'Impact, Charcoal, sans-serif' => 'Impact',
			'"Lucida Sans Unicode", "Lucida Grande", sans-serif' => 'Lucida Sans Unicode',
			'Tahoma, Geneva, sans-serif' => 'Tahoma',
			'"Trebuchet MS", Helvetica, sans-serif' => 'Trebuchet MS',
			'Verdana, Geneva, sans-serif' => 'Verdana',
			'"Courier New", Courier, monospace' => 'Courier New',
			'"Lucida Console", Monaco, monospace' => 'Lucida Console',
		);
		$wp_customize->add_setting( 'MyThemeName_body_fonts', array(
				'default' => 'Arial',
				'transport' => 'refresh',
			)
		);
		$wp_customize->add_control( 'MyThemeName_body_fonts', array(
				'type' => 'select',
				'description' => __( 'Select your desired font for the body.', 'MyThemeName' ),
				'section' => 'police_de_caracteres',
				'choices' => $font_choices
			)
		);

		// drop-down menu for titles fonts
		$wp_customize->add_setting( 'MyThemeName_titles_fonts', array(
				'default' => 'Arial',
				'transport' => 'refresh',
			)
		);
		$wp_customize->add_control( 'MyThemeName_titles_fonts', array(
				'type' => 'select',
				'description' => __( 'Select your desired font for titles.', 'MyThemeName' ),
				'section' => 'police_de_caracteres',
				'choices' => $font_choices
			)
		);
}
add_action( 'customize_register', 'MyThemeName_customize_register' );

/**
* Add the font selection in styles via the style.css file
*/
function add_font() {
	wp_enqueue_style('add-style', get_template_directory_uri() . '/style.css');
  $font_titles = get_theme_mod('MyThemeName_titles_fonts');
  $font_body = get_theme_mod('MyThemeName_body_fonts');
  $custom_css = "
								body{
									font-family: {$font_body};
								}
								h1,h2,h3,h4{
									font-family: {$font_titles};
								}";
  wp_add_inline_style( 'add-style', $custom_css );
}
add_action('wp_enqueue_scripts', 'add_font');

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function MyThemeName_customize_preview_js() {
	wp_enqueue_script( 'MyThemeName-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'MyThemeName_customize_preview_js' );
