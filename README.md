# Wordpress : customizer.php add a menu for standard font

## Context
For my work, I worked on the design of a wordpress theme creating eco-designed website. Eco-design involves the use of only standard font.
So I thought about the implementation of a feature that allows the user to choose a font among the standard.

## Wordpress theme or child Theme
You can work on a theme like on a child theme.

⚠️ If you work on a theme (created by yourself or by a third party). Pay attention to the position you will give to your new section
```php
'priority' => 10,
```
## What we want to do
#### what and where
![open customise](https://gitlab.com/lora0marie/wordpress-customizer-standard-font/raw/master/img/customise.png)

#### Before and After
![customise sans et avec la nouvel section](https://gitlab.com/lora0marie/wordpress-customizer-standard-font/raw/master/img/avant-apres.png)

#### The result
![the new section](https://gitlab.com/lora0marie/wordpress-customizer-standard-font/raw/master/img/end.png)

# The code
To set up our new section we need 4 things.
* add_section
* the font list
* add_setting
* add_control

These 4 things are to be added inside the following function  
:smiley: If you are working on a blank file you will obviously need to add it

#### customizer file base
Loads the $wp_customize object  
[wordpress codex explanation](https://codex.wordpress.org/Theme_Customization_API#Part_1:_Defining_Settings.2C_Controls.2C_Etc.)
```php
  function MyThemeName_customize_register( $wp_customize ) {
    // the 4 things
  }
  add_action( 'customize_register', 'MyThemeName_customize_register' );
```

### add_section
[wordpress codex explanation](https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section)
```php
$wp_customize->add_section( 'font_section', array(
    'title'       => __( 'Fonts', 'MyThemeName' ),
    'priority'    => 25, // WARNING: there may be another section at the same location
  ) );
```

### font list
```php
$font_choices = array(
  'Georgia, serif' => 'Georgia',
  '"Palatino Linotype", "Book Antiqua", Palatino, serif' => 'Palatino',
  ...
);
```

### add_setting
[wordpress codex explanation](https://codex.wordpress.org/Class_Reference%5CWP_Customize_Manager%5Cadd_setting)
```php
$wp_customize->add_setting( 'MyThemeName_body_fonts', array(
    'default' => 'Arial',
    'transport' => 'refresh',
  )
);
```

### add_control
[wordpress codex explanation](https://codex.wordpress.org/Class_Reference%5CWP_Customize_Manager%5Cadd_control)
```php
$wp_customize->add_control( 'MyThemeName_body_fonts', array(
    'type' => 'select',
    'description' => __( 'Select your desired font for the body.', 'MyThemeName' ),
    'section' => 'font_section',
    'choices' => $font_choices
  )
);
```

## apply the css
There are several methods to integrate the css. The one used to add it to the style file select rather than just add in the header (as explained in the documentation [link](https://codex.wordpress.org/Theme_Customization_API#Part_2:_Generating_Live_CSS))
```php
function apply_css() {
	wp_enqueue_style('add-style', get_template_directory_uri() . '/style.css');
  $font = get_theme_mod('perfecto_test_body_fonts');
  $custom_css = "
                body{
	                font-family: {$font};
                }";
  wp_add_inline_style( 'add-style', $custom_css );
}
add_action('wp_enqueue_scripts', 'apply_css');
```
## Little extra - reload changes asynchronously
Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
```php
function perfecto_test_customize_preview_js() {
	wp_enqueue_script( 'perfecto_test-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'perfecto_test_customize_preview_js' );
```
# Ajouter un deuxiéme onglet
## onglet selection police pour les titles
In the function ```MyThemeName_customizer_register``` add an ```add_setting``` and an ```add_control```.And then it will be necessary to modify the integration function.
```php
$wp_customize->add_setting( 'MyThemeName_titles_fonts', array(
    'default' => 'Arial',
    'transport' => 'refresh',
  )
);
```
```php
$wp_customize->add_control( 'MyThemeName_titles_fonts', array(
    'type' => 'select',
    'description' => __( 'Select your desired font for titles.', 'MyThemeName' ),
    'section' => 'police_de_caracteres',
    'choices' => $font_choices
  )
);
```
After that, it will retrieve the value of the new font and then integrate it for the titles (h1, h2, h3, h4, h5)
```php
function add_font() {
	wp_enqueue_style('add-style', get_template_directory_uri() . '/style.css');
  $font_titles = get_theme_mod('MyThemeName_titles_fonts'); // add this line
  $font_body = get_theme_mod('MyThemeName_body_fonts');
  // and add the 2nd css block
  $custom_css = "
                body{
                  font-family: {$font_body};
                }
                h1,h2,h3,h4{
                  font-family: {$font_titles};
                }";
  wp_add_inline_style( 'add-style', $custom_css );
}
add_action('wp_enqueue_scripts', 'add_font');
```

Marie LORA 🦝